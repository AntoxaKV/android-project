package ioDataModel;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import tools.DBConnection;

/**
 * Created by Drifty on 01.02.2017.
 */

public class IOObject {
    public static String ID_COL = "_id";
    public static String ID_COL_STORAGE = "id_storage";
    public static String NAME_COL = "name";
    public static String nameOfTable = "object";

    private SQLiteDatabase db;

    public IOObject(DBConnection dbConnection){
        db = dbConnection.getWritableDatabase();
    }

    public Cursor getAllObjects(int id_storage){
        return db.query(nameOfTable, null, ID_COL_STORAGE + " = " + id_storage, null, null, null, NAME_COL);
    }

    public long createObject (int id_storage, String name) throws SQLiteConstraintException {
        if(!isUniqueName(id_storage, name)){
            throw new SQLiteConstraintException();
        }
        ContentValues object = new ContentValues();
        object.put(ID_COL_STORAGE, id_storage);
        object.put(NAME_COL, name);
        return db.insertOrThrow(nameOfTable, null, object);
    }

    public String getNameObject(int id){
        Cursor cursor = db.query(nameOfTable, new String[]{NAME_COL}, ID_COL + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public int deleteObject (int id){
        return db.delete(nameOfTable, ID_COL + " = " + id, null);
    }

    public int updateObject (int id ,int id_storage, String name) throws SQLiteConstraintException{
        if(!isUniqueName(id_storage, name)){
            throw new SQLiteConstraintException();
        }
        ContentValues storage = new ContentValues();
        storage.put(NAME_COL, name);
        return db.update(nameOfTable, storage, ID_COL + " = " + id, null);
    }

    private boolean isUniqueName (int id_storage, String name){
        Cursor cursor = db.query(nameOfTable, null, ID_COL_STORAGE + " = ? and " + NAME_COL + " = ?", new String[]{String.valueOf(id_storage), name}, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        return count == 0;
    }
}
