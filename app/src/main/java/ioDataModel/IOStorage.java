package ioDataModel;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import tools.DBConnection;

/**
 * Created by Drifty on 02.01.2017.
 */

public class IOStorage {

    public static String ID_COL = "_id";
    public static String NAME_COL = "name";
    public static String nameOfTable = "storage";

    private SQLiteDatabase db;

    public IOStorage(DBConnection dbConnection){
        db = dbConnection.getWritableDatabase();
    }

    public Cursor getAllStorage(){
        return db.query(nameOfTable, null, null, null, null, null, NAME_COL);
    }

    public long createStorage (String name) throws SQLiteConstraintException {
        ContentValues storage = new ContentValues();
        storage.put(NAME_COL, name);
        return db.insertOrThrow(nameOfTable, null, storage);
    }

    public int deleteStorage (int id){
        return db.delete(nameOfTable, ID_COL + " = " + id, null);
    }

    public int updateStorage (int id, String name) throws SQLiteConstraintException{
        ContentValues storage = new ContentValues();
        storage.put(NAME_COL, name);
        return db.update(nameOfTable, storage, ID_COL + " = " + id, null);
    }

    public Cursor searchStorage(String name){
        return db.query(nameOfTable, null, "name like '%" + name + "%'", null, null, null, NAME_COL);
    }
}
