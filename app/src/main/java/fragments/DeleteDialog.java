package fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import activity.R;

/**
 * Created by Drifty on 31.01.2017.
 */

public class DeleteDialog extends AppCompatDialogFragment implements DialogInterface.OnClickListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_for_delete)
                .setNegativeButton(R.string.no, this)
                .setPositiveButton(R.string.yes, this)
                .setMessage(R.string.message_for_delete);
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == Dialog.BUTTON_POSITIVE) {
            ((CallBack) getActivity()).onPositiveAnswer();
        }
    }

    public interface CallBack{
        void onPositiveAnswer ();
    }
}
