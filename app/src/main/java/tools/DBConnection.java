package tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ioDataModel.IOObject;
import ioDataModel.IOStorage;

/**
 * Created by Drifty on 02.01.2017.
 */

public class DBConnection extends SQLiteOpenHelper {



    public static String nameDB = "ObjectInStorage";
    public static int version = 1;
    private String createStorage = "CREATE TABLE [storage](\n" +
            "    [_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \n" +
            "    [name] VARCHAR(50) NOT NULL UNIQUE);";
    private String createObject = "CREATE TABLE [object] (\n" +
            "  [_id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n" +
            "  [id_storage] INTEGER NOT NULL REFERENCES [storage]([_id]) ON DELETE CASCADE, \n" +
            "  [name] VARCHAR(50) NOT NULL);";

    public DBConnection(Context ctx){
        super(ctx, nameDB, null, 1);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createStorage);
        sqLiteDatabase.execSQL(createObject);

        long id_storage [] = new long[5];
        ContentValues storage = new ContentValues();
        for(int i = 0; i < 5; i++){
            storage.put(IOStorage.NAME_COL, "Хранилище  №" + (i+1));
            id_storage [i] = sqLiteDatabase.insertOrThrow(IOStorage.nameOfTable, null, storage);
        }

        ContentValues object = new ContentValues();
        for(int i = 0; i < 5; i++){
            for (int j = 1; j < 6; j++){
                object.put(IOObject.ID_COL_STORAGE, (int)id_storage[i]);
                object.put(IOObject.NAME_COL, "Вещь  №" + j);
                sqLiteDatabase.insertOrThrow(IOObject.nameOfTable, null, object);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}