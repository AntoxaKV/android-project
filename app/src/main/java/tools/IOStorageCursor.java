package tools;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import ioDataModel.IOStorage;

/**
 * Created by Drifty on 03.01.2017.
 */

public class IOStorageCursor extends CursorLoader {

    private IOStorage storage;
    private String name;

    public IOStorageCursor(Context context, IOStorage storage) {
        super(context);
        this.storage = storage;
    }


    @Override
    public Cursor loadInBackground() {
        if(name != null && !name.isEmpty()){
            return storage.searchStorage(name);
        }
        return storage.getAllStorage();
    }

    public void setAllStorage(){
        name = null;
        forceLoad();
    }

    public void searchStoage(String name){
        this.name = name;
        forceLoad();
    }
}
