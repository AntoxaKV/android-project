package tools;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import activity.MainActivity;
import activity.R;
import fragments.DeleteDialog;

/**
 * Created by Drifty on 07.01.2017.
 */

public class ActionModeDeleteStorage implements ActionMode.Callback {

    private MainActivity activity;
    private ActionMode actionMode;
    public ActionModeDeleteStorage(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.getMenuInflater().inflate(R.menu.delete, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.delete){
            DeleteDialog dialog = new DeleteDialog();
            dialog.show(activity.getSupportFragmentManager(), "dialog");
            this.actionMode = actionMode;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        activity.setDeleteMode(false);
        activity.setDefaultAdapter();
        activity.getLoaderOfCursor().setAllStorage();
    }

    public ActionMode getActionMode() {
        return actionMode;
    }
}
