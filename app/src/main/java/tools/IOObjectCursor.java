package tools;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import ioDataModel.IOObject;
import ioDataModel.IOStorage;

/**
 * Created by Drifty on 01.02.2017.
 */

public class IOObjectCursor extends CursorLoader {

    private IOObject object;
    private int id_storage;

    public IOObjectCursor(Context context, IOObject object, int id_storage) {
        super(context);
        this.object = object;
        this.id_storage = id_storage;
    }


    @Override
    public Cursor loadInBackground() {
        return object.getAllObjects(id_storage);
    }
}
