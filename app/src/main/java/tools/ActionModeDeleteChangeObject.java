package tools;

import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import activity.ObjectsActivity;
import activity.R;
import fragments.DeleteDialog;

/**
 * Created by Drifty on 03.02.2017.
 */

public class ActionModeDeleteChangeObject implements ActionMode.Callback {

    private ObjectsActivity objectsActivity;
    private ActionMode actionMode;

    public ActionModeDeleteChangeObject(ObjectsActivity objectsActivity){
        this.objectsActivity = objectsActivity;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.getMenuInflater().inflate(R.menu.menu_obj_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.delete){
            this.actionMode = actionMode;
            DeleteDialog dialog = new DeleteDialog();
            dialog.show(objectsActivity.getSupportFragmentManager(), "dialog");
        }else if(menuItem.getItemId() == R.id.change){
            objectsActivity.startChangeObjectDialog();
        }

        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        objectsActivity.setDeleteMode(false);
        objectsActivity.setDefaultAdapter();
        objectsActivity.getLoader().forceLoad();
    }

    public ActionMode getActionMode() {
        return actionMode;
    }
}
