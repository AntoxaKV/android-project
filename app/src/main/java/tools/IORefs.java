package tools;

import android.app.Application;

import ioDataModel.IOObject;
import ioDataModel.IOStorage;

/**
 * Created by Drifty on 04.01.2017.
 */

public class IORefs extends Application {
    private IOStorage ioStorage;
    private IOObject ioObject;

    public IOObject getIoObject() {
        return ioObject;
    }

    public void setIoObject(IOObject ioObject) {
        this.ioObject = ioObject;
    }

    public IOStorage getIoStorage() {
        return ioStorage;
    }

    public void setIoStorage(IOStorage ioStorage) {
        this.ioStorage = ioStorage;
    }

}
