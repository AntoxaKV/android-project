package activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import fragments.DeleteDialog;
import ioDataModel.IOObject;
import ioDataModel.IOStorage;

import tools.ActionModeDeleteStorage;
import tools.DBConnection;
import tools.IORefs;
import tools.IOStorageCursor;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DeleteDialog.CallBack {

    private ListView listOfstorages;
    private TextView emptyMsgListStorage;
    private FloatingActionButton addStorageButton;
    private ActionModeDeleteStorage deleteMenu = new ActionModeDeleteStorage(this);
    private SearchView searchView;

    private DBConnection dbc;
    private IOStorage ios;
    private SimpleCursorAdapter scAdapter;
    private IOStorageCursor loaderOfCursor;

    String LOG_TAG = "myMSG";
    private boolean isSearchMode = false;
    private boolean isDeleteMode = false;


    public void setDeleteMode(boolean deleteMode) {
        isDeleteMode = deleteMode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.storages_activity);

        dbc = new DBConnection(this);
        ios = new IOStorage(dbc);
        IOObject ioo = new IOObject(dbc);
        ((IORefs)getApplicationContext()).setIoStorage(ios);
        ((IORefs)getApplicationContext()).setIoObject(ioo);

        initViewsOfStorages();
    }

    private void initViewsOfStorages(){
        addStorageButton = (FloatingActionButton)findViewById(R.id.addStorage);
        addStorageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dialog = new Intent(getBaseContext(), AddChangeDialog.class);
                dialog.putExtra("requestCode", AddChangeDialog.ADD_STORAGE);
                startActivityForResult(dialog, AddChangeDialog.ADD_STORAGE);
            }
        });

        listOfstorages = (ListView) findViewById(R.id.storage);
        setDefaultAdapter();
        loaderOfCursor = (IOStorageCursor)getSupportLoaderManager().initLoader(0, null, this);
        loaderOfCursor.setAllStorage();
        listOfstorages.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isSearchMode){
                    startActionMode(deleteMenu);
                    setDeleteAdapter();
                    loaderOfCursor.setAllStorage();
                    isDeleteMode = true;
                }
                return false;
            }
        });
        listOfstorages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isDeleteMode){
                    startActivityForResult(new Intent(MainActivity.this,
                            ObjectsActivity.class).putExtra("id", l).putExtra("name", ((TextView)view).getText()), ObjectsActivity.IS_NEED_UPDATE_LIST);
                }
            }
        });
        View emptyMsg = getLayoutInflater().inflate(R.layout.msgforlist, null);
        addContentView(emptyMsg, listOfstorages.getLayoutParams());
        emptyMsgListStorage = (TextView) emptyMsg.findViewById(R.id.informMsg);
        emptyMsgListStorage.setText(R.string.no_storage);
        listOfstorages.setEmptyView(emptyMsg);
    }

    public void setDefaultAdapter(){
        scAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null,
                new String[]{ios.NAME_COL}, new int[]{android.R.id.text1}, 0);
        listOfstorages.setAdapter(scAdapter);
        listOfstorages.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        addStorageButton.setVisibility(View.VISIBLE);
    }

    public void setDeleteAdapter(){
        scAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_multiple_choice, null,
                new String[]{ios.NAME_COL}, new int[]{android.R.id.text1}, 0);
        listOfstorages.setAdapter(scAdapter);
        listOfstorages.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        addStorageButton.setVisibility(View.INVISIBLE);
    }

    public void deleteStorage(){
         long ids [] = listOfstorages.getCheckedItemIds();
         for (long i : ids){
             ios.deleteStorage((int)i);
         }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        searchView = (SearchView) menu.findItem(R.id.searchItem).getActionView();
        searchView.setQueryHint(getResources().getString(R.string.search));
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                addStorageButton.setVisibility(View.VISIBLE);
                isSearchMode = false;
                emptyMsgListStorage.setText(R.string.no_storage);
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.isEmpty()){
                    loaderOfCursor.setAllStorage();
                }else{
                    loaderOfCursor.searchStoage(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return onQueryTextSubmit(newText);
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSearchMode = true;
                addStorageButton.setVisibility(View.INVISIBLE);
                emptyMsgListStorage.setText(R.string.not_find_storage);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AddChangeDialog.ADD_STORAGE && resultCode == AddChangeDialog.AGREE_DIALOG){
            loaderOfCursor.setAllStorage();
        }
        if(requestCode == ObjectsActivity.IS_NEED_UPDATE_LIST && resultCode == ObjectsActivity.UPDATE_LIST){
            if(isSearchMode) loaderOfCursor.searchStoage(String.valueOf(searchView.getQuery()));
            else loaderOfCursor.setAllStorage();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportLoaderManager().destroyLoader(0);
        dbc.close();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new IOStorageCursor(this, ios);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        scAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        scAdapter.swapCursor(null);
    }

    public IOStorageCursor getLoaderOfCursor() {
        return loaderOfCursor;
    }

    @Override
    public void onPositiveAnswer() {
        deleteStorage();
        getLoaderOfCursor().setAllStorage();
        isDeleteMode = false;
        deleteMenu.getActionMode().finish();
    }
}
