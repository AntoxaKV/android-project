package activity;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ioDataModel.IOObject;
import ioDataModel.IOStorage;
import tools.IORefs;

public class AddChangeDialog extends AppCompatActivity implements View.OnClickListener {

    public static final int ADD_STORAGE = 1;
    public static final int CHANGE_STORAGE = 2;
    public static final int ADD_OBJECT = 3;
    public static final int CHANGE_OBJECT = 4;
    public static final int AGREE_DIALOG = 5;
    public static final int CANCEL_DIALOG = 6;

    private TextView error;
    private TextView textView;
    private EditText name;
    private Button cancel;
    private Button agree;

    private int requestCode;
    private String errorEmptyText;
    private String errorUniqueText;
    private String textViewString;
    private String titleText;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_change);
        error = (TextView) findViewById(R.id.error);
        textView = (TextView)findViewById(R.id.textView);
        name = (EditText) findViewById(R.id.name);
        cancel = (Button) findViewById(R.id.cancel);
        agree = (Button) findViewById(R.id.agree);

        cancel.setOnClickListener(this);
        agree.setOnClickListener(this);

        requestCode = getIntent().getIntExtra("requestCode", 0);
        initText();
    }

    private void initText (){
        if(requestCode == ADD_STORAGE || requestCode == CHANGE_STORAGE){
            errorEmptyText = getString(R.string.error_empty_name_storage);
            errorUniqueText = getString(R.string.error_not_unique_name_storage);
            textViewString = getString(R.string.text_view_storage);
            if(requestCode == ADD_STORAGE){
                titleText = getString(R.string.add_storage);
            }else {
                titleText = getString(R.string.change_storage);
                name.setText(getIntent().getStringExtra("name"));
            }
        }else{
            errorEmptyText = getString(R.string.error_empty_name_object);
            errorUniqueText = getString(R.string.error_not_unique_name_object);
            textViewString = getString(R.string.text_view_object);
            if(requestCode == ADD_OBJECT){
                titleText = getString(R.string.add_object);
            }else{
                titleText = getString(R.string.change_object);
                name.setText(getIntent().getStringExtra("name"));
            }
        }

        setTitle(titleText);
        textView.setText(textViewString);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.agree){
            String name = this.name.getText().toString();
            if(name.isEmpty()){
                error.setText(errorEmptyText);
            }else{
                try {
                    if(requestCode == ADD_STORAGE || requestCode == CHANGE_STORAGE){
                        IOStorage ios = ((IORefs)getApplicationContext()).getIoStorage();
                        if(requestCode == ADD_STORAGE){
                            ios.createStorage(name);
                            setResult(AGREE_DIALOG);
                        }else{
                            ios.updateStorage(getIntent().getIntExtra("id", 0), name);
                            setResult(AGREE_DIALOG, new Intent().putExtra("newName", name));
                        }
                    }else{
                        IOObject ioo = ((IORefs)getApplicationContext()).getIoObject();
                        if(requestCode == ADD_OBJECT){
                            ioo.createObject(getIntent().getIntExtra("id_storage", 0), name);
                        }else{
                            ioo.updateObject(getIntent().getIntExtra("id", 0), getIntent().getIntExtra("id_storage", 0), name);
                        }
                        setResult(AGREE_DIALOG);
                    }
                    finish();
                }catch (SQLiteConstraintException e){
                    error.setText(errorUniqueText);
                }
            }
        }else{
            setResult(CANCEL_DIALOG);
            finish();
        }
    }
}
