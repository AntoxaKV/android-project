package activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import fragments.DeleteDialog;
import ioDataModel.IOObject;
import tools.ActionModeDeleteChangeObject;
import tools.IOObjectCursor;
import tools.IORefs;


/**
 * Created by Drifty on 30.01.2017.
 */

public class ObjectsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DeleteDialog.CallBack{

    private int idStorage;
    private String nameStorage;
    private IOObject ioo;

    public static final int IS_NEED_UPDATE_LIST = 0;
    public static final int UPDATE_LIST = 1;
    public static final int NOT_UPDATE_LIST = 2;

    private ListView listOfOjects;
    private TextView emptyMsgText;
    private FloatingActionButton addObjectButton;
    private ActionModeDeleteChangeObject actionModeDCO = new ActionModeDeleteChangeObject(this);
    private Menu menuActionMode;
    private SimpleCursorAdapter adapterForDelete;
    private SimpleCursorAdapter adapterForShow;
    private IOObjectCursor loader;

    private boolean isDeleteMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.objects_activity);
        idStorage = (int)getIntent().getLongExtra("id", 0);
        nameStorage = getIntent().getStringExtra("name");
        setTitle(nameStorage);
        setResult(NOT_UPDATE_LIST);

        ioo = ((IORefs)getApplicationContext()).getIoObject();

        adapterForShow = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null,
                new String[]{ioo.NAME_COL}, new int[]{android.R.id.text1}, 0);
        adapterForDelete = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_multiple_choice, null,
                new String[]{ioo.NAME_COL}, new int[]{android.R.id.text1}, 0);

        initViewsOfObjects();

    }

    private void initViewsOfObjects(){
        addObjectButton = (FloatingActionButton)findViewById(R.id.addObject);
        addObjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(ObjectsActivity.this, AddChangeDialog.class)
                        .putExtra("id_storage", idStorage).putExtra("requestCode", AddChangeDialog.ADD_OBJECT), AddChangeDialog.ADD_OBJECT);
            }
        });

        listOfOjects = (ListView) findViewById(R.id.objects);
        listOfOjects.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isDeleteMode){
                    isDeleteMode = true;
                    setDeleteAdapter();
                    loader.forceLoad();
                    menuActionMode = startActionMode(actionModeDCO).getMenu();
                    menuActionMode.setGroupVisible(R.id.change_group, false);
                }
                return false;
            }
        });
        listOfOjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(isDeleteMode){
                    if(listOfOjects.getCheckedItemIds().length == 1){
                        menuActionMode.setGroupVisible(R.id.change_group, true);
                    }else{
                        menuActionMode.setGroupVisible(R.id.change_group, false);
                    }
                }
            }
        });
        Bundle id_bundle = new Bundle();
        id_bundle.putInt("idStorage", idStorage);
        loader = (IOObjectCursor)getSupportLoaderManager().initLoader(0, id_bundle, this);
        setDefaultAdapter();
        View emptyMsg = getLayoutInflater().inflate(R.layout.msgforlist, null);
        addContentView(emptyMsg, listOfOjects.getLayoutParams());
        emptyMsgText = (TextView) emptyMsg.findViewById(R.id.informMsg);
        emptyMsgText.setText(R.string.no_object);
        listOfOjects.setEmptyView(emptyMsg);
    }

    public void setDefaultAdapter(){
        listOfOjects.setAdapter(adapterForShow);
        listOfOjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        addObjectButton.setVisibility(View.VISIBLE);
    }

    public void setDeleteAdapter(){
        listOfOjects.setAdapter(adapterForDelete);
        listOfOjects.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        addObjectButton.setVisibility(View.INVISIBLE);
    }

    public void deleteObject(){
        long ids [] = listOfOjects.getCheckedItemIds();
        for (long i : ids){
            ioo.deleteObject((int)i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_obj_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.change:
                Intent change = new Intent(ObjectsActivity.this, AddChangeDialog.class);
                change.putExtra("requestCode", AddChangeDialog.CHANGE_STORAGE);
                change.putExtra("id", idStorage);
                change.putExtra("name", nameStorage);
                startActivityForResult(change, AddChangeDialog.CHANGE_STORAGE);
                break;
            case R.id.delete:
                DeleteDialog dialog = new DeleteDialog();
                dialog.show(getSupportFragmentManager(), "dialog");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == AddChangeDialog.CHANGE_STORAGE && resultCode == AddChangeDialog.AGREE_DIALOG){
            setTitle(data.getStringExtra("newName"));
            setResult(UPDATE_LIST);
        }

        if(requestCode == AddChangeDialog.CHANGE_OBJECT && resultCode == AddChangeDialog.AGREE_DIALOG){
            loader.forceLoad();
            listOfOjects.clearChoices();
            menuActionMode.setGroupVisible(R.id.change_group, false);
        }

        if(requestCode == AddChangeDialog.ADD_OBJECT && resultCode == AddChangeDialog.AGREE_DIALOG){
            loader.forceLoad();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void startChangeObjectDialog(){
        Intent change = new Intent(ObjectsActivity.this, AddChangeDialog.class);
        change.putExtra("requestCode", AddChangeDialog.CHANGE_OBJECT);
        int id = (int)listOfOjects.getCheckedItemIds()[0];
        change.putExtra("id", id);
        change.putExtra("id_storage", idStorage);
        change.putExtra("name", ioo.getNameObject(id));
        startActivityForResult(change, AddChangeDialog.CHANGE_OBJECT);
    }

    @Override
    public void onPositiveAnswer() {
        if(isDeleteMode){
            deleteObject();
            setDefaultAdapter();
            loader.forceLoad();
            actionModeDCO.getActionMode().finish();
        } else {
            ((IORefs)getApplicationContext()).getIoStorage().deleteStorage(idStorage);
            setResult(UPDATE_LIST);
            finish();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new IOObjectCursor(this, ioo, args.getInt("idStorage"));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(isDeleteMode){
            adapterForShow.swapCursor(null);
            adapterForDelete.swapCursor(data);
        }else{
            adapterForDelete.swapCursor(null);
            adapterForShow.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapterForShow.swapCursor(null);
        adapterForDelete.swapCursor(null);
    }

    @Override
    protected void onDestroy() {
        getSupportLoaderManager().destroyLoader(0);
        super.onDestroy();
    }

    public void setDeleteMode(boolean deleteMode) {
        isDeleteMode = deleteMode;
    }

    public IOObjectCursor getLoader() {
        return loader;
    }
}
